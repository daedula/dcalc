import std.stdio;

import calculator : Calculator;

void main()
{
	Calculator calc = new Calculator();
	calc.run();
}
