# dcalc

This is a calculator thing implemented in D for shits and giggles.
This is also my first project in D.

## build it

You need a D compiler and DUB installed on your system. [See here.](https://dlang.org/)

```shell
dub # run it
```

## usage

The calculator supports:
- `+`, `-`, `*`, `/`, `%` operators
- Exponents via `^`
- `PI`
- Parentheses
- Variables
    - Set variables by prefixing your expression with `[<variable name here>]`

## limitations

- Functions (or a lacking thereof)
- The calculator is not omniscient.

## the verdict

D's a fun language. I kinda like it.
