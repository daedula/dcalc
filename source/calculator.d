module calculator;

import std.stdio;
import std.string;
import std.array;
import std.math;
import std.algorithm : map, all, reduce, filter;
import std.container : SList;
import std.conv : to;

enum CharType {
    None,
    Digit,
    Letter,
    Operator
}

abstract class ExpressionPart {
    public bool isLiteral() @property const pure nothrow {
        return cast(LiteralExpressionPart)(this) !is null;
    }

    public double asLiteral() @property const pure {
        return (cast(LiteralExpressionPart)(this)).value;
    }
    
    public bool isToken() @property const pure nothrow {
        return cast(TokenExpressionPart)(this) !is null;
    }

    public Token asToken() @property const pure {
        return (cast(TokenExpressionPart)(this)).token;
    }
    
    public bool isSetVariable() @property const pure nothrow {
        return cast(SetsVariableExpressionPart)(this) !is null;
    }

    public string asSetVariable() @property const pure {
        return (cast(SetsVariableExpressionPart)(this)).name;
    }
}

enum Token {
    Equals,
    Plus,
    Minus,
    Multiply,
    Divide,
    Modulus,
    Exponent,
    ParenOpen,
    ParenClose
}

class TokenExpressionPart : ExpressionPart {
    public Token token;

    this(Token token) {
        this.token = token;
    }
}

class LiteralExpressionPart : ExpressionPart {
    public double value;

    this(double value) {
        this.value = value;
    }
}

class SetsVariableExpressionPart : ExpressionPart {
    public string name;

    this(string name) {
        this.name = name;
    }
}

abstract class Expression {
    abstract double resolve() const;

    public bool isLiteral() @property const pure nothrow {
        return cast(Literal)(this) !is null;
    }

    public Literal asLiteral() @property const pure {
        return cast(Literal)(this);
    }
    
    public bool isParens() @property const pure nothrow {
        return cast(ParenExpression)(this) !is null;
    } 

    public const(Expression) unwrap() @property const pure {
        return (cast(ParenExpression)(this)).contained;
    }

    public Expression wrap() @property const pure {
        return new ParenExpression(this);
    }
    
    public bool isOperator() @property const pure nothrow {
        return cast(Operator)(this) !is null;
    }

    public Operator asOperator() @property const pure {
        return cast(Operator)(this);
    }
    
    public bool isExponent() @property const pure nothrow {
        return cast(ExponentOperator)(this) !is null;
    }

    public ExponentOperator asExponent() @property const pure {
        return cast(ExponentOperator)(this);
    }
}

class Literal : Expression {
    private double value;

    this(double value) {
        this.value = value;
    }

    override double resolve() const {
        return value;
    }
}

abstract class Operator : Expression {
    public Expression[] operands;

    this(Expression[] operands) {
        this.operands = operands;
    }

    public void append(Expression expr) {
        auto appender = appender(operands);
        appender.put(expr);
        operands = appender.data;
    } 

    abstract double reduce(double a, double b) const;

    double resolve(double[] values) const {
        return values.reduce!((a, b) => reduce(a, b));
    }
    
    override double resolve() const {
        return operands
            .map!(op => op.resolve())
            .reduce!((a, b) => reduce(a, b));
    }
}

class ParenExpression : Expression {
    const Expression contained;

    this(const Expression contained) pure nothrow {
        this.contained = contained;
    }

    override double resolve() const {
        return contained.resolve();
    }
}

class PlusOperator : Operator {
    this(Expression[] operands) {
        super(operands);
    }

    override double reduce(double a, double b) const {
        return a + b;
    }
}

class MinusOperator : Operator {
    this(Expression[] operands) {
        super(operands);
    }

    override double reduce(double a, double b) const {
        return a - b;
    }
}

class MultiplyOperator : Operator {
    this(Expression[] operands) {
        super(operands);
    }

    override double reduce(double a, double b) const {
        return a * b;
    }
}

class DivideOperator : Operator {
    this(Expression[] operands) {
        super(operands);
    }

    override double reduce(double a, double b) const {
        return a / b;
    }
}

class ExponentOperator : Expression {
    private Expression value, exponent;

    this(Expression value, Expression exponent) {
        this.value = value;
        this.exponent = exponent;
    }

    override double resolve() const {
        return value.resolve().pow(exponent.resolve());
    }
}

class ModulusOperator : Operator {
    this(Expression[] operands) {
        super(operands);
    }

    override double reduce(double a, double b) const {
        return a % b;
    }
}

public class Calculator {
    private immutable double[string] constants;
    private double[string] variables;

    this() {
        constants = [
            "PI": PI
        ];
    }

    public void run() {
        for(int i = 1; true; i++) {
            writef(" %4d  |> ", i);
            string line = readln().chomp();

            auto split = appender!(string[])();
            auto buf = appender!(char[])();
            CharType type = CharType.None;

            foreach (char ch; line)
            {
                switch(ch) {
                    case '0': .. case '9':
                        if(type != CharType.Digit) {
                            if(buf.data.length > 0) {
                                split.put(buf.data.idup);
                                buf.clear();
                            }

                            type = CharType.Digit;
                        }
                        
                        buf.put(ch);
                        break;
                    case 'a': .. case 'z':
                    case 'A': .. case 'Z':
                        if(type != CharType.Letter) {
                            if(buf.data.length > 0) {
                                split.put(buf.data.idup);
                                buf.clear();
                            }

                            type = CharType.Letter;
                        }
                        
                        buf.put(ch);
                        break;
                    case ' ':
                        if(type != CharType.None) {
                            if(buf.data.length > 0) {
                                split.put(buf.data.idup);
                                buf.clear();
                            }

                            type = CharType.None;
                            buf.put(ch);
                        }
                        
                        break;
                    case '+':
                        if(buf.data.length > 0) {
                            split.put(buf.data.idup);
                            buf.clear();
                        }

                        type = CharType.Operator;
                        split.put("+");
                        break;
                    case '-':
                        if(buf.data.length > 0) {
                            split.put(buf.data.idup);
                            buf.clear();
                        }

                        type = CharType.Operator;
                        split.put("-");
                        break;
                    case '*':
                        if(buf.data.length > 0) {
                            split.put(buf.data.idup);
                            buf.clear();
                        }

                        type = CharType.Operator;
                        split.put("*");
                        break;
                    case '/':
                        if(buf.data.length > 0) {
                            split.put(buf.data.idup);
                            buf.clear();
                        }

                        type = CharType.Operator;
                        split.put("/");
                        break;
                    case '^':
                        if(buf.data.length > 0) {
                            split.put(buf.data.idup);
                            buf.clear();
                        }

                        type = CharType.Operator;
                        split.put("^");
                        break;
                    case '%':
                        if(buf.data.length > 0) {
                            split.put(buf.data.idup);
                            buf.clear();
                        }

                        type = CharType.Operator;
                        split.put("%");
                        break;
                    case '(':
                        if(buf.data.length > 0) {
                            split.put(buf.data.idup);
                            buf.clear();
                        }

                        type = CharType.Operator;
                        split.put("(");
                        break;
                    case ')':
                        if(buf.data.length > 0) {
                            split.put(buf.data.idup);
                            buf.clear();
                        }

                        type = CharType.Operator;
                        split.put(")");
                        break;
                    case '[':
                        if(buf.data.length > 0) {
                            split.put(buf.data.idup);
                            buf.clear();
                        }

                        type = CharType.Letter;
                        buf.put("[");
                        break;
                    case ']':
                        if(buf.data.length > 0) {
                            buf.put("]");
                            split.put(buf.data.idup);
                            buf.clear();
                        }

                        type = CharType.Letter;
                        break;
                    default:
                        throw new Exception(format("Illegal character %c", ch));
                }
            }

            if(buf.data.length > 0) {
                split.put(buf.data.idup);
                buf.clear();
            }

            auto tokenized = split.data.filter!(i => i != " ").map!((tk) {
                if(tk.all!(ch => ch >= 'A' && ch <= 'Z')) {
                    if(tk !in constants) {
                        stderr.writefln("[%4d] !! Constant %s not found; replacing with zero", i, tk);
                        return cast(ExpressionPart)(new LiteralExpressionPart(0));
                    }
                    return cast(ExpressionPart)(new LiteralExpressionPart(constants[tk]));
                }

                if(tk.all!(ch => ch >= 'a' && ch <= 'z')) {
                    if(tk !in variables) {
                        stderr.writefln("[%4d] !! Variable %s not found; replacing with zero", i, tk);
                        return cast(ExpressionPart)(new LiteralExpressionPart(0));
                    }
                    return cast(ExpressionPart)(new LiteralExpressionPart(variables[tk]));
                }

                if(tk.startsWith("[") && tk.endsWith("]")) {
                    auto variableName = tk[1 .. $ - 1];
                    return cast(ExpressionPart)(new SetsVariableExpressionPart(variableName));
                }

                switch(tk) {
                    case "+":
                        return cast(ExpressionPart)(new TokenExpressionPart(Token.Plus));
                    case "-":
                        return cast(ExpressionPart)(new TokenExpressionPart(Token.Minus));
                    case "*":
                        return cast(ExpressionPart)(new TokenExpressionPart(Token.Multiply));
                    case "/":
                        return cast(ExpressionPart)(new TokenExpressionPart(Token.Divide));
                    case "^":
                        return cast(ExpressionPart)(new TokenExpressionPart(Token.Exponent));
                    case "%":
                        return cast(ExpressionPart)(new TokenExpressionPart(Token.Modulus));
                    case "(":
                        return cast(ExpressionPart)(new TokenExpressionPart(Token.ParenOpen));
                    case ")":
                        return cast(ExpressionPart)(new TokenExpressionPart(Token.ParenClose));
                    default:
                        return cast(ExpressionPart)(new LiteralExpressionPart(to!double(tk)));
                }
            });

            Expression parsed = null;
            auto stack = SList!Expression();
            auto setVariables = SList!string();

            foreach (token; tokenized) {
                if (token.isSetVariable) {
                    setVariables.insertFront(token.asSetVariable);
                } else if (token.isLiteral) {
                    if (parsed is null) {
                        parsed = new Literal(token.asLiteral);
                    } else {
                        if (parsed.isLiteral || parsed.isParens) {
                            parsed = new MultiplyOperator([parsed, new Literal(token.asLiteral)]);
                        } else if (parsed.isExponent) {
                            parsed.asExponent.exponent = new Literal(token.asLiteral);
                            parsed = parsed.wrap;
                        } else if (parsed.isOperator) {
                            parsed.asOperator.append(new Literal(token.asLiteral));
                        }
                    }
                } else if (token.isToken) {
                    switch (token.asToken) {
                        case Token.Exponent:
                            parsed = new ExponentOperator(parsed.asLiteral, null);
                            break;
                        case Token.Plus:
                            if(parsed !is null) parsed = new PlusOperator([parsed]);
                            else parsed = new PlusOperator([new Literal(0)]); // support unary plus
                            break;
                        case Token.Minus:
                            if(parsed !is null) parsed = new MinusOperator([parsed]);
                            else parsed = new MinusOperator([new Literal(0)]); // support unary minus
                            break;
                        case Token.Multiply:
                            if(parsed !is null) parsed = new MultiplyOperator([parsed]);
                            else parsed = new MultiplyOperator([]);
                            break;
                        case Token.Divide:
                            if(parsed !is null) parsed = new DivideOperator([parsed]);
                            else parsed = new DivideOperator([]);
                            break;
                        case Token.Modulus:
                            if(parsed !is null) parsed = new ModulusOperator([parsed]);
                            else parsed = new ModulusOperator([]);
                            break;
                        case Token.ParenOpen:
                            stack.insertFront(parsed);
                            parsed = null;
                            break;
                        case Token.ParenClose:
                            if (stack.empty) {
                                stderr.writefln("[%4d] !! Some end parentheses do not have a corresponding opener", i);
                                break;
                            }

                            auto old = stack.front();
                            stack.removeFront();
                            
                            if (old is null) {
                                parsed = parsed.wrap;
                            } else {
                                if (old.isLiteral || old.isParens) {
                                    parsed = new MultiplyOperator([old.wrap, parsed.wrap]);
                                } else if (old.isExponent) {
                                    old.asExponent.exponent = parsed.wrap;
                                    parsed = old.wrap;
                                } else if (old.isOperator) {
                                    old.asOperator.append(parsed.wrap);
                                    parsed = old;
                                }
                            }

                            break;
                        default:
                            break;
                    }
                }
            }

            if (parsed !is null) {
                auto value = parsed.resolve();
                writefln("[%4d] <= %s", i, value);

                foreach (variable; setVariables)
                {
                    variables[variable] = value;
                    writefln("[%4d] -- Set variable '%s' to %s", i, variable, value);
                }
            }
        }
    }
}
